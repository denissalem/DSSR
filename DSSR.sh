#! /bin/bash

# Copyright 2021 Denis Salem
#
# This file is part of DSSR.
#
# DSSR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# DSSR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DSSR. If not, see <http://www.gnu.org/licenses/>.

# Project based on
# - https://funprojects.blog/2021/04/11/a-web-server-in-1-line-of-bash/
# - https://www.linuxjournal.com/content/parsing-rss-news-feed-bash-script

trap ctrl_c INT

function ctrl_c() {
    dssr_clear_sessions
    exit 0;
}

dssr_clear_sessions() {
	rm /tmp/dssr-* -f 2> /dev/null;
}

dssr_echo_header() {
    header=$(cat << EOF
<!DOCTYPE html>
<html>
    <head>
        <title>DSSR</title>
        <style type="text/css">
            a
                {color: white;
                text-decoration: none;}
            body
                {color: #FFF;
                font-family: DejaVu Sans;
                font-weight: lighter;
                text-shadow: 1px 2px 0px #000, 1px 1px 0px #000;
                background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABABAMAAABYR2ztAAAAyHpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjabVDBDcMwCPx7io5gwCZ4HKdJpG7Q8XsOJEqinuQzcNaBSev3s6XXAFNJpU6mTTUDpZXGHYFlR9+Zctl5RwkJ+a2eToFREtziqWm8P+p0GvjVEdWLkb1DmO9Ciw5sD6NoJGMiRrCEUQsjYRcoDLp/K2uz6fqFec13mJ80aHtzG7U6u/bMy4TtLRV9hHkVkgxmYR9AxqEkHYGBSRQPaY+rKFhEYxIs5N+eDqQffQ5aV4bjnO0AAAGEaUNDUElDQyBwcm9maWxlAAB4nH2RPUjDQBiG36aKRSoKdhBxyFCdLIiKOmoVilAh1AqtOphc+gdNGpIUF0fBteDgz2LVwcVZVwdXQRD8AXFzc1J0kRK/SwotYrzjuIf3vvfl7jtAqJeZZnWMAZpum6lEXMxkV8WuV4TQR3MaQZlZxpwkJeE7vu4R4PtdjGf51/05etScxYCASDzLDNMm3iCe2rQNzvvEEVaUVeJz4lGTLkj8yHXF4zfOBZcFnhkx06l54gixWGhjpY1Z0dSIJ4mjqqZTvpDxWOW8xVkrV1nznvyF4Zy+ssx1WkNIYBFLkCBCQRUllGEjRrtOioUUncd9/IOuXyKXQq4SGDkWUIEG2fWD/8Hv3lr5iXEvKRwHOl8c52MY6NoFGjXH+T52nMYJEHwGrvSWv1IHZj5Jr7W06BHQuw1cXLc0ZQ+43AEGngzZlF0pSEvI54H3M/qmLNB/C3SveX1rnuP0AUhTr5I3wMEhMFKg7HWfd4fa+/ZvTbN/P2HOcqAFrfdrAAAPi2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNC40LjAtRXhpdjIiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgeG1sbnM6aXB0Y0V4dD0iaHR0cDovL2lwdGMub3JnL3N0ZC9JcHRjNHhtcEV4dC8yMDA4LTAyLTI5LyIKICAgIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIgogICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgIHhtbG5zOnBsdXM9Imh0dHA6Ly9ucy51c2VwbHVzLm9yZy9sZGYveG1wLzEuMC8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOmM3OWZkMDM4LWYwYTAtNDAxZS04MzdhLTAzMzgwYzBkYTMxZiIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxOWYwNjMwMS01M2FiLTRlN2ItOWFlMy1kOTkyYzg0YzJlODEiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0OGM0YzYzZC1jOWUyLTQ1ZGUtYTllMC01YTQ3ZmUzOGUwODMiCiAgIEdJTVA6QVBJPSIyLjAiCiAgIEdJTVA6UGxhdGZvcm09IkxpbnV4IgogICBHSU1QOlRpbWVTdGFtcD0iMTYyOTEzMjk5NzgxNTY2NyIKICAgR0lNUDpWZXJzaW9uPSIyLjEwLjIyIgogICBkYzpGb3JtYXQ9ImltYWdlL3BuZyIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiPgogICA8aXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvbkNyZWF0ZWQ+CiAgIDxpcHRjRXh0OkxvY2F0aW9uU2hvd24+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpMb2NhdGlvblNob3duPgogICA8aXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpBcnR3b3JrT3JPYmplY3Q+CiAgIDxpcHRjRXh0OlJlZ2lzdHJ5SWQ+CiAgICA8cmRmOkJhZy8+CiAgIDwvaXB0Y0V4dDpSZWdpc3RyeUlkPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo1MTZiNzY4NC1iY2U4LTRhOGMtYTRmMy1iMjA4M2UyZWMwNGMiCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoTGludXgpIgogICAgICBzdEV2dDp3aGVuPSIrMDI6MDAiLz4KICAgIDwvcmRmOlNlcT4KICAgPC94bXBNTTpIaXN0b3J5PgogICA8cGx1czpJbWFnZVN1cHBsaWVyPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VTdXBwbGllcj4KICAgPHBsdXM6SW1hZ2VDcmVhdG9yPgogICAgPHJkZjpTZXEvPgogICA8L3BsdXM6SW1hZ2VDcmVhdG9yPgogICA8cGx1czpDb3B5cmlnaHRPd25lcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkNvcHlyaWdodE93bmVyPgogICA8cGx1czpMaWNlbnNvcj4KICAgIDxyZGY6U2VxLz4KICAgPC9wbHVzOkxpY2Vuc29yPgogIDwvcmRmOkRlc2NyaXB0aW9uPgogPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+5URGQAAAABhQTFRFRUdESUpIT1FPUlRRVFZTW11aXmBdX2Jf4VC2kwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+UIEBA4JaqJ9wMAAAWuSURBVEjHFZUxt6M6EoSFDJ4UZGBSqaTWTa9ZezYFGbwpyOCX2hp74nl39/3/7cl8DvJRqavqaxFLV2tPdjFHaTdtNhds8gY2G2NS2AtMKjXBAaN/nOiXk+ZRGdnQ7gYtLyYJJSNWqyUBBKPkfwoadkss4yn/SHAQ4QUgtckA5jINz/jGx5aX92RDp6A6EY2RVg7KfRat1dMlVCiH2QvpAhT/V8DRit/1IJvHwpKcvpiruw7vBOi1S5oPdCib6J7RkYpv5W/uU5mfGqZre8ALlwPXFaZGY2NwoXNwEYXHy4wbixBgeUsNcgiwWAJfS5fmLjUo+9RYxOmzQX09hXT44ofDWolaRzw+nNk+rmYVioIJAyuaeYCJForVSuDJyVNn4I5C6d51zphzy8dyRTo5zUPr9G3X5alMIhZ7NX+6g9OHCak3bZVYz99tY9Lh2W+FiPHWU6vW6h0U9L9tOGnIcjeHLvSO2pvQbk/G9Goi41SjTggN5mn1bnRbhWMhmnmW9PahJpuGpYc0ChOba6Op7W5bxLo2L7Afi4I51N6vlI428jTg9Dpj5VEv38yZHpfom3UxTh7l2WWotOmnQDIKWB/ML+ucCYtyDgO9A97ZqSVf4pKCcKTDXu0L3aGIozJv3URrQ1CruyIaLZQ599ua/vmo+WadqCyWv625VymD3GX+KkKhprnYuW5Dr/YS08shO8UT3uF3XJ0UeEQ/pRml1NTfniQp9xv0sTa9HwSbBarUDT8+r5HIc26/8EZvGh9BnXvXi0CzBlQ8FJ6g4oSVXQVaB0jHZp6Wl1DBcDckh4fUWvr7VIz6H0dsFRqJnD8udlPlW056DXoy4/oASX0mTCY8urgTR2oQzs0cSYLuFGkyzXpGWXFiC6k3cUMNTOuOk4IjR7lDs5Q1pliQSrFwosITKtBVfkfYwUkOkzxfQtj9RK/lOwqPBgk2JfU0SclF39I9+qO8O0PXdIwimYvVcE1IQX10CzWkOREU4mculmxyQqGqCtQBR/sjVDXpljBLPDtK/9AbWgxuXJ+oFC6W5+/UX8dEReroqGavlPFiV0Ue2s9UcxbiOATU/l+RO0YrqfBsIa4bOGBnE5tMZfIEw+WLSgmjs/AnmCLZA+oFB8xh/rlIOwbJsQr/q/eJsUWNMKpHrM1Ytr8aNz1hMKuZ4Le1yeERRB48OLRYI0YkyZ5S2OtO6r5M/AXiWbGGFJgRlbXZu1vlFKLD7OjFckiKYlxqTkJ1KbguXxN0vHCpQV63B6WRBEOuZ+mjXKIT7sz1rl3+XXrsY2QtUkgU29KJOdodu10O5Dv89UgHOYeMNKKIWbLF9Ku5MSwdpjNTx/xmnC3f1R0TrHA3UJKmN1730O7RMpwcddmKQVkmiWAG125xBQnXmta4QMQ6hhciV9N0pUhKBwN66XvLoKqJf/fwowWkzqkehQleO5KWwpCKuyq8wh0IkYfTJLE0wkH5zqTZKe63es+m1xnnzkAy1N6OMSjnCcxEVpSz2ZRP+1fCNTexK3DOtOjfP4ypHIWkvnIY9xOrVppvyPm9Nu1FwehVL+ghdPoPh7WYm9ig9bG/uJqe/AplRqUIVfDMLdNudLOR7bpRzt4e/uSB1d3UYVSyy8OZW82BE3HHCyhStwmXyIzQ+KXCoJ8dno6yGK4yfbpYY4Vw4ZhnS7MeUn3jsrDT3Mdyeaj2h1ngRoEyn//7GnlB9bnO+cTgt+3u/eemTox83lnX+OYyma9wivv0ItkoFXmTTBzHgKHi8i6T+ubAXMkilH66p5IfVOVWXRusqxgvjQw3Q6d+esWaCEM+cwLeUc9LxJF3FhujeUDBZ4yB4OpT4g1OSEV7g1qDKMeEj85pjS+NPOyCVY2nqdLv4CRTRmxdUArlN+/2329JRn5b4Ce9vAr+N1Wd4IiFo97CYP5cFXuygytVr5gqmbej+T80PWtbfQTuDQAAAABJRU5ErkJggg==);}
            h1
                {font-family: DejaVu Sans;
                font-weight: lighter;}
            ul
                {list-style-type:none;}
        </style>
    </head>
    <body>
EOF);
    echo $header;
}

dssr_echo_footer() {
    header=$(cat << EOF
    </body>
</html>
EOF);
    echo $header;
}

dssr_feed_has_been_updated() {
    feed_url="$1"
}

dssr_filter_cdata() {
    sed 's/<\!\[CDATA\[//' | sed 's/\]\]>//'
}

dssr_get_feed_titles() {
    export -f dssr_xml_get_rss_feed_title
    dssr_get_feeds | xargs  -d '\n' -L1 | echo
}

dssr_process_feeds() {
	dssr_conf_path="$1/.dssr.conf"
    dssr_cache_path="$1/.dssr_caches"
    
    dssr_echo_header
    
    if  [ ! -f "$dssr_conf_path" ]; then
		echo "There is no user's DSSR configuration file."
		return;
	fi
	echo "<!-- read feeds from $dssr_conf_path -->"
    ################ iterate through categories
    for category in `cat $dssr_conf_path | xargs -d '\n' -L1 | awk '$2 {print $2}' | sort | uniq`; do
        echo "<h1>$category</h1>";
        echo "<ul>";
        ################ pick feeds for given category
        for feed in $(grep $category <(cat $dssr_conf_path | xargs -d '\n' -L1 | awk '$3 {print $2"\t"$3}') | xargs -L1 | cut -d ' ' -f 2); do
            feed_name=$(curl $feed | dssr_xml_get_rss_feed_title);
            site=$(curl $feed | dssr_xml_get_rss_feed_link);
            echo "<li><a href=\"$site\">$feed_name</a></li>";
        done
        echo "</ul>";
    done
    
    dssr_echo_footer
}

dssr_xml_get_next () {
	local IFS='>' 
    read -d '<' TAG VALUE
}

dssr_xml_get_rss_feed_title() {
    dssr_filter_cdata | while dssr_xml_get_next ; do echo "$TAG{$VALUE}" ; done | grep "title" | grep -v "^/" | head -1 | sed s/"title{"// | sed s/"}$"//
}

dssr_xml_get_rss_feed_link() {
    dssr_filter_cdata | while dssr_xml_get_next ; do echo "$TAG{$VALUE}" ; done | grep "link" | grep -v "^/" | grep -v "atom:" | head -1 | sed s/"link{"// | sed s/"}$"//
}

dssr_xml_get_rss_pubdate() {
	while dssr_xml_get_next ; do echo "$TAG{$VALUE}" ; done | grep "pubDate" | grep -v "^/"
}

dssr_response() {
		connection_port=$(cat /tmp/dssr-$1 | grep -i "Connection from" | tail -1 | cut -d ':' -f 3 | tr -d '.')
		client_pid=$(netstat -tpn 2> /dev/null | grep $connection_port | grep -v "ncat" | awk '{print $7}' | tr -d '[[:alpha:]][/]')
		user_name=$(ps -u -p $client_pid | tail -1 | awk '{print $1}')
		user_home=$(eval echo "~$user_name")
        request=$(cat /tmp/dssr-request-$1 | head -1 | cut -d ' ' -f 2 ;)
        request=${request:1} # remove slash /
        
        if [ -z "$request" ]
        then
            page=$(dssr_process_feeds $user_home; echo $request);
        else
            page=$(echo "Must process $request")
        fi
            
        echo -ne "HTTP/1.0 200 OK\r\nContent-Length: $(echo $page | wc -c)\r\n\r\n";
        echo "$page";
        rm "/tmp/dssr-request-$1"
        rm "/tmp/dssr-$1"
}

dssr_server() {
    while true; do
		session_id=$(cat /dev/random | tr -dc '0-9' | head --bytes 32);
		ncat -l 127.0.0.1 8080 --exec "$0 response $session_id" -o /tmp/dssr-request-$session_id  &> /tmp/dssr-$session_id 
	done
}

if [[ $1 == "response" ]]; then
	dssr_response $2;
else
    dssr_clear_sessions
	dssr_server;
fi
